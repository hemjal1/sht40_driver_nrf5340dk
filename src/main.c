#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>

#include<stdio.h>

#include "sht4x.h"


int main(void)
{
	printk("\n\n ***** SHT40 driver for %s *****\n\n", CONFIG_BOARD);
	sensirion_i2c_init();
	if(sht4x_probe() != STATUS_OK) {
        printk("SHT40 sensor initialization failed\n");
	}
	else { printk("SHT40 sensor initialization successful!\n\n");}

	while (1) {
        int32_t temperature, humidity;
        int8_t ret = sht4x_measure_blocking_read(&temperature, &humidity);   // Measure temperature and relative humidity
        if (ret == STATUS_OK) {
            printk("SHT40 measured Temperature: %2.3f degreeCelsius, "
                   "measured Humidity: %2.2f \%\n",
                   (float)temperature/1000.0f , (float)humidity/1000.0f );
        } else {
            printk("Error reading measurement\n");
        }

        sensirion_sleep_usec(2000000); /* sleep 2s */
    }
	return 0;
}
