
#include "sht4x.h"

/* all measurement commands return T (CRC) RH (CRC) */
#define SHT4X_CMD_MEASURE_HPM 0xFD
#define SHT4X_CMD_MEASURE_LPM 0xE0
#define SHT4X_CMD_READ_SERIAL 0x89
#define SHT4X_CMD_DURATION_USEC 1000

#define SHT4X_ADDRESS 0x44

static uint8_t sht4x_cmd_measure = SHT4X_CMD_MEASURE_HPM;
static uint16_t sht4x_cmd_measure_delay_us = SHT4X_MEASUREMENT_DURATION_USEC;

uint16_t rev16(uint16_t x) {
  return (x << 8) | (x >>8);
}

int16_t sht4x_measure_blocking_read(int32_t* temperature, int32_t* humidity) {
    int16_t ret;

    ret = sht4x_measure();
    if (ret)
        return ret;
    sensirion_sleep_usec(sht4x_cmd_measure_delay_us);
    return sht4x_read(temperature, humidity);
}

int16_t sht4x_measure(void) {
    return sensirion_i2c_write(SHT4X_ADDRESS, &sht4x_cmd_measure, 1);
}

int16_t sht4x_read(int32_t* temperature, int32_t* humidity) {
    uint16_t words[3];
    int16_t ret = sensirion_i2c_read(SHT4X_ADDRESS, words, SENSIRION_RESPONSE_SIZE);
   // printk("SHT4x Sensor raw data is %2x %2x %2x \n", words[0], words[1], words[2]);
    /**
     * formulas for conversion of the sensor signals, optimized for fixed point
     * algebra:
     * Temperature = 175 * S_T / 65535 - 45
     * Relative Humidity = 125 * (S_RH / 65535) - 6
     */
    uint16_t temperature_raw = rev16(words[0]);
    *temperature = ((21875 * (int32_t)temperature_raw) >> 13) - 45000;
   // printk("SHT4x Sensor Temperature %d \n", *temperature);
    uint16_t buf = (words[1] & 0xFF00) | (words[2] & 0x00FF);
    *humidity = ((15625 * (int32_t)buf) >> 13) - 6000;
   // printk("SHT4x Sensor Humidity %d \n", *humidity);

    return ret;
}

int16_t sht4x_probe(void) {
    uint32_t serial;

    return sht4x_read_serial(&serial);
}

void sht4x_enable_low_power_mode(uint8_t enable_low_power_mode) {
    if (enable_low_power_mode) {
        sht4x_cmd_measure = SHT4X_CMD_MEASURE_LPM;
        sht4x_cmd_measure_delay_us = SHT4X_MEASUREMENT_DURATION_LPM_USEC;
    } else {
        sht4x_cmd_measure = SHT4X_CMD_MEASURE_HPM;
        sht4x_cmd_measure_delay_us = SHT4X_MEASUREMENT_DURATION_USEC;
    }
}

int16_t sht4x_read_serial(uint32_t* serial) {
    const uint8_t cmd = SHT4X_CMD_READ_SERIAL;
    int16_t ret;
    uint16_t serial_words[3];

    ret = sensirion_i2c_write(SHT4X_ADDRESS, &cmd, 1);
    if (STATUS_FAIL == ret)        return STATUS_FAIL;

    sensirion_sleep_usec(SHT4X_CMD_DURATION_USEC);
    k_msleep(1000);

    ret = sensirion_i2c_read(SHT4X_ADDRESS, (uint8_t *) serial_words, SENSIRION_RESPONSE_SIZE);
  //  printk("SHT4x Serial data is %2x %2x %2x \n", serial_words[0], serial_words[1], serial_words[2]);
    uint16_t buf = (serial_words[1] & 0xFF00) | (serial_words[2] & 0x00FF);
    *serial = ((uint32_t)rev16(serial_words[0]) << 16) | (uint32_t)buf;
    printk("Found SHT40 sensor With ID: 0x%04X\n", *serial);
    return ret;
}

const char* sht4x_get_driver_version(void) {
    return "1";
}

uint8_t sht4x_get_configured_address(void) {
    return SHT4X_ADDRESS;
}
