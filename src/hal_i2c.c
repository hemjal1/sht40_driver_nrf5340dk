#include "hal_i2c.h"


#define I2C1_NODE DT_NODELABEL(sht40sensor)

static const struct i2c_dt_spec dev_i2c = I2C_DT_SPEC_GET(I2C1_NODE);

void sensirion_i2c_init(void) {
  
  if (!device_is_ready(dev_i2c.bus)) {
    printk("I2C bus %s is not ready!\n\r", dev_i2c.bus->name);
    return;
  }
}

int8_t sensirion_i2c_read(uint8_t address, uint8_t* data, uint16_t count) {
  
  uint8_t value[count];
  uint8_t  ret = i2c_read_dt(&dev_i2c, value, count);
  memcpy(data, value, count);
		if (ret != 0)
		{
			printk("Failed to read from I2C device address. %x \n", dev_i2c.addr);
      return STATUS_FAIL;
		}
    else return ret;
  
}

void sensirion_sleep_usec(uint32_t useconds) {

  k_usleep(useconds);

}

int8_t sensirion_i2c_write(uint8_t address, const uint8_t* data,uint16_t count) {

  uint8_t ret = i2c_write_dt(&dev_i2c, data, count);
		if (ret != 0)
		{
			printk("Failed to write to I2C device address. %x \n", dev_i2c.addr);
      return STATUS_FAIL;
		}
		  return STATUS_OK;
  
}

uint8_t sensirion_i2c_generate_crc(const uint8_t* data, uint16_t count) {
  
    uint16_t current_byte;
    uint8_t crc = CRC8_INIT;
    uint8_t crc_bit;

    /* calculates 8-Bit checksum with given polynomial */
    for (current_byte = 0; current_byte < count; ++current_byte) {
        crc ^= (data[current_byte]);
        for (crc_bit = 8; crc_bit > 0; --crc_bit) {
            if (crc & 0x80)
                crc = (crc << 1) ^ CRC8_POLYNOMIAL;
            else
                crc = (crc << 1);
        }
    }
    return crc;
}

int8_t sensirion_i2c_check_crc(const uint8_t* data, uint16_t count, uint8_t checksum) {

    if (sensirion_i2c_generate_crc(data, count) != checksum)
        return STATUS_FAIL;
    return STATUS_OK;
}