# SHT40 driver for nrf5340dk

## Motivation

The main purpose of this driver frimware is to provide easy to use interface for nrf5340 microcntroller and the sht40 temperature sensor.

## Project Environment

- OS: UBUNTU
- IDE: VS Code
- Dk: nrf5340-DK
- nrf connect sdk version: west v2.5.1


### What is SHT40?

SHT40 is a digital sensor platform for measuring relative humidity and temperature at different
accuracy classes. Its I2C interface provides several preconfigured I2C addresses while maintaining
an ultra-low power budget. 
Key features:

- 4th Generation sensor
- High-Accuracy
- Ultra-Low-Power
- Accuracies ΔRH = ±1.0 %RH, ΔT = ±0.1 °C
- VDD = 1.08 V … 3.6 V
- Avg. current: 0.4 μA, Idle current: 80 nA
- Operating range: 0 … 100 %RH, −40…125 °C


### Datasheet for SHT40
Datasheet for SHT40 can be downloaded directly from the following  link:
https://sensirion.com/media/documents/33FD6951/63E1087C/Datasheet_SHT4x_1.pdf

### How the driver is configured

First of all I took sample code from the sensirion github webpages. Then I modified the library code and fit it into the nrf ecosystem.


### Driver implementation steps:

- First create a sample project for nrf-5340dk
- Add the I2C driver library
- Add the sensor library
- Special attention given to the parts of the project so that I can add other sensor libraries as well.

### How i2c protocol works:

I2C communcation has 2 main lines 
- SDA for data communication
- SCL for clocking

In addion the VDD and VSS(GND) should be connected between the master and the slave. connection between MCU and the sensor given below:


![Alt text](assests/mcu_SHT4x_connection.png "I2C connection")

### Development kit

nrf-5340 DK was used to communicate with the sensor:

![Alt text](assests/pcb.png "dev board")


### Sensor kit

![Alt text](assests/SHT40.jpg "SENSOR")

## Connection Diagram

| nrf-5340 DK | SHT40   |
|-------------|---------|
| GND         | GND     |
| VDD         | VDD/3V6 |
| P1.02       | SDA     |
| P1.03       | SCL     |


### Final picture

![Alt text](assests/mcu_sensor.jpg "connection diagram")


### Sample Output:

``` c
*** Booting nRF Connect SDK v2.5.1 ***


 ***** SHT40 driver for nrf5340dk_nrf5340_cpuapp *****

Found SHT40 sensor With ID: 0x1174B62A
SHT40 sensor initialization successful!

SHT40 measured Temperature: 22.606 degreeCelsius, measured Humidity: 35.65 %
SHT40 measured Temperature: 22.627 degreeCelsius, measured Humidity: 35.71 %
SHT40 measured Temperature: 22.617 degreeCelsius, measured Humidity: 35.70 %
SHT40 measured Temperature: 22.593 degreeCelsius, measured Humidity: 35.69 %
SHT40 measured Temperature: 22.601 degreeCelsius, measured Humidity: 35.68 %
SHT40 measured Temperature: 22.611 degreeCelsius, measured Humidity: 35.71 %
SHT40 measured Temperature: 22.633 degreeCelsius, measured Humidity: 35.71 %

```

### logic analyzer output

- Sensor address

![Alt text](assests/address.png "Sensor address information")

- Sensor raw measurement data

![Alt text](assests/measurement.png "Sensor raw measurement data")